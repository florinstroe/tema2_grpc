﻿using Grpc.Net.Client;
using System;
using System.Globalization;
using System.Threading.Tasks;

namespace ZodiacClient
{
    class Program
    {
        static bool isDateValid(string dateAsString)
        {
            DateTime temporaryDate;
            string[] allowedDateFormats = { "MM/dd/yyyy", "M/d/yyyy", "MM/d/yyyy", "M/dd/yyyy" };
            if (DateTime.TryParseExact(dateAsString, allowedDateFormats, new CultureInfo("en-US"), DateTimeStyles.None, out temporaryDate))
            {
                return true;
            }
            return false;
        }
        static async Task Main(string[] args)
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Zodiac.ZodiacClient(channel);

            while (true)
            {
                Console.Write("What's your date of birth? ");
                string dateInput = Console.ReadLine();
                if (isDateValid(dateInput))
                {
                    var reply = await client.ZodiacAsync(new ServerRequest { DateOfBirth = dateInput });
                    Console.WriteLine("Your zodiac sign is " + reply.Sign);
                }
                else
                {
                    Console.WriteLine("Date is not valid! Please use the following format: 01/01/1999 or 1/1/1999!");
                }
            }
        }
    }
}
