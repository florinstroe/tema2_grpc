﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ZodiacServer
{
    public class WinterService : Winter.WinterBase
    {
        public override Task<WinterServerReply> Winter(WinterServerRequest request, ServerCallContext context)
        {
            DateTime dateFromMainService = DateTime.Parse(request.DateOfBirth);
            var winterZodiacs = Data.Utilities.GetZodiacDataFromFile("Winter.txt");
            foreach (Tuple<string, string, string> zodiac in winterZodiacs)
            {
                DateTime firstDate = DateTime.Parse(zodiac.Item1 + "/" + dateFromMainService.Year);
                DateTime endDate = DateTime.Parse(zodiac.Item2 + "/" + dateFromMainService.Year);
                string sign = zodiac.Item3;
                if (dateFromMainService >= firstDate && dateFromMainService <= endDate)
                {
                    Data.Zodiacs.ZodiacEnum signEnum = Enum.Parse<Data.Zodiacs.ZodiacEnum>(sign);
                    return Task.FromResult(new WinterServerReply
                    {
                        Sign = (WinterServerReply.Types.zodiac_sign)signEnum
                    });
                }
            }

            return Task.FromResult(new WinterServerReply
            {
                Sign = (WinterServerReply.Types.zodiac_sign)Data.Zodiacs.ZodiacEnum.ERROR
            });
        }
    }
}
