﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ZodiacServer
{
    public class SpringService : Spring.SpringBase
    {
        public override Task<SpringServerReply> Spring(SpringServerRequest request, ServerCallContext context)
        {
            DateTime dateFromMainService = DateTime.Parse(request.DateOfBirth);
            var SpringZodiacs = Data.Utilities.GetZodiacDataFromFile("Spring.txt");
            foreach (Tuple<string, string, string> zodiac in SpringZodiacs)
            {
                DateTime firstDate = DateTime.Parse(zodiac.Item1 + "/" + dateFromMainService.Year);
                DateTime endDate = DateTime.Parse(zodiac.Item2 + "/" + dateFromMainService.Year);
                string sign = zodiac.Item3;
                if (dateFromMainService >= firstDate && dateFromMainService <= endDate)
                {
                    Data.Zodiacs.ZodiacEnum signEnum = Enum.Parse<Data.Zodiacs.ZodiacEnum>(sign);
                    return Task.FromResult(new SpringServerReply
                    {
                        Sign = (SpringServerReply.Types.zodiac_sign)signEnum
                    });
                }
            }

            return Task.FromResult(new SpringServerReply
            {
                Sign = (SpringServerReply.Types.zodiac_sign)Data.Zodiacs.ZodiacEnum.ERROR
            });
        }
    }
}
