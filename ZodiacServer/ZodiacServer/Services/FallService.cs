﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ZodiacServer
{
    public class FallService : Fall.FallBase
    {
        public override Task<FallServerReply> Fall(FallServerRequest request, ServerCallContext context)
        {
            DateTime dateFromMainService = DateTime.Parse(request.DateOfBirth);
            var FallZodiacs = Data.Utilities.GetZodiacDataFromFile("Fall.txt");
            foreach (Tuple<string, string, string> zodiac in FallZodiacs)
            {
                DateTime firstDate = DateTime.Parse(zodiac.Item1 + "/" + dateFromMainService.Year);
                DateTime endDate = DateTime.Parse(zodiac.Item2 + "/" + dateFromMainService.Year);
                string sign = zodiac.Item3;
                if (dateFromMainService >= firstDate && dateFromMainService <= endDate)
                {
                    Data.Zodiacs.ZodiacEnum signEnum = Enum.Parse<Data.Zodiacs.ZodiacEnum>(sign);
                    return Task.FromResult(new FallServerReply
                    {
                        Sign = (FallServerReply.Types.zodiac_sign)signEnum
                    });
                }
            }

            return Task.FromResult(new FallServerReply
            {
                Sign = (FallServerReply.Types.zodiac_sign)Data.Zodiacs.ZodiacEnum.ERROR
            });
        }
    }
}
