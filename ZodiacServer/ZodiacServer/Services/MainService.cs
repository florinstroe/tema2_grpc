﻿using Grpc.Core;
using Grpc.Net.Client;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace ZodiacServer
{
    public class MainService : Zodiac.ZodiacBase
    {
        static Data.Zodiacs.ZodiacEnum CallSummerService(string date)
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Summer.SummerClient(channel);
            var reply = client.Summer(new SummerServerRequest { DateOfBirth = date });
            return (Data.Zodiacs.ZodiacEnum)reply.Sign;
        }

        static Data.Zodiacs.ZodiacEnum CallFallService(string date)
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Fall.FallClient(channel);
            var reply = client.Fall(new FallServerRequest { DateOfBirth = date });
            return (Data.Zodiacs.ZodiacEnum)reply.Sign;
        }

        static Data.Zodiacs.ZodiacEnum CallWinterService(string date)
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Winter.WinterClient(channel);
            var reply = client.Winter(new WinterServerRequest { DateOfBirth = date });
            return (Data.Zodiacs.ZodiacEnum)reply.Sign;
        }

        static Data.Zodiacs.ZodiacEnum CallSpringService(string date)
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Spring.SpringClient(channel);
            var reply = client.Spring(new SpringServerRequest { DateOfBirth = date });
            return (Data.Zodiacs.ZodiacEnum)reply.Sign;
        }

        public override Task<ServerReply> Zodiac(ServerRequest request, ServerCallContext context)
        {
            Data.Zodiacs.ZodiacEnum returnedSign = Data.Zodiacs.ZodiacEnum.ERROR;

            string dateFromClient = request.DateOfBirth;
            switch (DateTime.Parse(dateFromClient).Month)
            {
                case 12:
                    returnedSign = CallWinterService(dateFromClient);
                    break;
                case 1:
                    returnedSign = CallWinterService(dateFromClient);
                    break;
                case 2:
                    returnedSign = CallWinterService(dateFromClient);
                    break;
                case 3:
                    returnedSign = CallSpringService(dateFromClient);
                    break;
                case 4:
                    returnedSign = CallSpringService(dateFromClient);
                    break;
                case 5:
                    returnedSign = CallSpringService(dateFromClient);
                    break;
                case 6:
                    returnedSign = CallSummerService(dateFromClient);
                    break;
                case 7:
                    returnedSign = CallSummerService(dateFromClient);
                    break;
                case 8:
                    returnedSign = CallSummerService(dateFromClient);
                    break;
                case 9:
                    returnedSign = CallFallService(dateFromClient);
                    break;
                case 10:
                    returnedSign = CallFallService(dateFromClient);
                    break;
                case 11:
                    returnedSign = CallFallService(dateFromClient);
                    break;
            }

            return Task.FromResult(new ServerReply
            {
                Sign = (ServerReply.Types.zodiac_sign)returnedSign
            }); ;
        }
    }
}
