﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ZodiacServer.Data
{
    public class Utilities
    {
        public static List<Tuple<string, string, string>> GetZodiacDataFromFile(string file)
        {
            List<Tuple<string, string, string>> zodiacData = new List<Tuple<string, string, string>>();
            String line;
            try
            {
                StreamReader sr = new StreamReader(@".\Data\" + file);
                line = sr.ReadLine();
                while (line != null)
                {
                    var aux = line.Split("|");
                    zodiacData.Add(new Tuple<string, string, string>(aux[0], aux[1], aux[2]));
                    line = sr.ReadLine();
                }
                sr.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
            return zodiacData;
        }
    }
}
