﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZodiacServer.Data
{
    public class Zodiacs
    {
        public enum ZodiacEnum
        {
            ARIES,
            TAURUS,
            GEMINI,
            CANCER,
            LEO,
            VIRGO,
            LIBRA,
            SCORPIO,
            SAGITTARIUS,
            CAPRICORN,
            AQUARIUS,
            PISCES,
            ERROR
        }
    }
}
